// DATABASE CONFIGURATIONS
// this is configurations to work with MongoDB engine

export default {
    enable: true, // enable database using in this project
    driver: 'mongoose',

    // connection settings
    host: 'localhost',
    port: '27017',
    dbName: 'test',
    user: '',
    pass: '',
};
