// ############################################################################
// # API SERVER STARTER PACK FOR
// # NODEJS + EXPRESS + MONGOOSE
// #
// # Author: ThinhIT (thinhit.net)
// # Email:  thinhnd.ict@gmail.com
// # Github: https://github.com/realThinhIT/nodeejs
// ############################################################################

global.__DIR_BASE   = __dirname + '/';
global.__DIR_APP    = __dirname + '/app/';
global.__DIR_CORE   = __dirname + '/core/';

require(__DIR_CORE + 'init');
